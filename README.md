# Elasticsearch

## Initialize
```
e := elasticsearch.Elasticsearch{Url:"http://127.0.0.1:9200"}
```

## What's implemented?
I implemented all cat apis from Elasticsearch, some cluster apis like clusterhealth and settings, send and bulk messages.

I just implemented stuff I needed. If you need anything more just text me or feel free to contribute.
