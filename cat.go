package elasticsearch

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

func (e Elasticsearch) Indices() ([]Index, error) {
	data, err := e.cat_get("_cat/indices?v")
	if err != nil {
		return nil, err
	}
	indices := []Index{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			index := Index{}
			index.Health = fdata[0]
			index.Status = fdata[1]
			index.Index = fdata[2]
			index.Uuid = fdata[3]
			index.PriString = fdata[4]
			index.Pri = toInt(fdata[4])
			index.RepString = fdata[5]
			index.Rep = toInt(fdata[5])
			index.DocsCountString = fdata[6]
			index.DocsCount = toInt(fdata[6])
			index.DocsDeletedString = fdata[7]
			index.DocsDeleted = toInt(fdata[7])
			index.StoreSizeString = fdata[8]
			index.StoreSize = toByte(fdata[8])
			index.PriStoreSizeString = fdata[9]
			index.PriStoreSize = toByte(fdata[9])
			indices = append(indices, index)
		}
	}
	return indices, nil
}

func (e Elasticsearch) Aliases() ([]Alias, error) {
	data, err := e.cat_get("_cat/aliases?v")
	if err != nil {
		return nil, err
	}
	aliases := []Alias{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			alias := Alias{}
			alias.Alias = fdata[0]
			alias.Index = fdata[1]
			alias.Filter = fdata[2]
			alias.RoutingIndex = fdata[3]
			alias.RoutingSearch = fdata[4]
			aliases = append(aliases, alias)
		}
	}
	return aliases, nil
}

func (e Elasticsearch) Allocation() ([]Allocation, error) {
	data, err := e.cat_get("_cat/allocation?v")
	if err != nil {
		return nil, err
	}
	allocations := []Allocation{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			allocation := Allocation{}
			allocation.ShardsString = fdata[0]
			allocation.Shards = toInt(fdata[0])
			allocation.DiskIndicesString = fdata[1]
			allocation.DiskIndices = toByte(fdata[1])
			allocation.DiskUsedString = fdata[2]
			allocation.DiskUsed = toByte(fdata[2])
			allocation.DiskAvailableString = fdata[3]
			allocation.DiskAvailable = toByte(fdata[3])
			allocation.DiskTotalString = fdata[4]
			allocation.DiskTotal = toByte(fdata[4])
			allocation.DiskPercentString = fdata[5]
			allocation.DiskPercent = toInt(fdata[5])
			allocation.Host = fdata[6]
			allocation.Ip = fdata[7]
			allocation.Node = fdata[8]
			allocations = append(allocations, allocation)
		}
	}
	return allocations, nil
}

func (e Elasticsearch) Count() (Count, error) {
	data, err := e.cat_get("_cat/count?v")
	if err != nil {
		return Count{}, err
	}
	count := Count{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			count.EpochString = fdata[0]
			i, err := strconv.ParseInt(fdata[0], 10, 64)
			if err != nil {
				panic(err)
			}
			count.Epoch = time.Unix(i, 0)
			count.Timestamp = fdata[1]
			count.CountString = fdata[2]
			count.Count = toInt(fdata[2])
		}
	}
	return count, nil
}

func (e Elasticsearch) CountIndex(index string) (Count, error) {
	data, err := e.cat_get(fmt.Sprintf("_cat/count/%s?v", index))
	if err != nil {
		return Count{}, err
	}
	count := Count{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			count.EpochString = fdata[0]
			count.Epoch = toTime(fdata[0])
			count.Timestamp = fdata[1]
			count.CountString = fdata[2]
			count.Count = toInt(fdata[2])
		}
	}
	return count, nil
}

func (e Elasticsearch) FieldData(args ...string) ([]FieldData, error) {
	url := "_cat/fielddata?v"
	if len(args) > 0 {
		url = fmt.Sprintf("_cat/fielddata/%s?v", strings.Join(args, ","))
	}
	data, err := e.cat_get(url)
	if err != nil {
		return nil, err
	}
	fieldData := []FieldData{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			fielddata := FieldData{}
			fielddata.Id = fdata[0]
			fielddata.Host = fdata[1]
			fielddata.Ip = fdata[2]
			fielddata.Node = fdata[3]
			fielddata.Field = fdata[4]
			fielddata.SizeString = fdata[5]
			fielddata.Size = toByte(fdata[5])
			fieldData = append(fieldData, fielddata)
		}
	}
	return fieldData, nil
}

func (e Elasticsearch) Health() (Health, error) {
	data, err := e.cat_get("_cat/health?v")
	if err != nil {
		return Health{}, err
	}
	health := Health{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			health.EpochString = fdata[0]
			health.Epoch = toTime(fdata[0])
			health.Timestamp = fdata[1]
			health.Cluster = fdata[2]
			health.Status = fdata[3]
			health.NodeTotalString = fdata[4]
			health.NodeTotal = toInt(fdata[4])
			health.NodeDataString = fdata[5]
			health.NodeData = toInt(fdata[5])
			health.ShardsString = fdata[6]
			health.Shards = toInt(fdata[6])
			health.PriString = fdata[7]
			health.Pri = toInt(fdata[7])
			health.ReloString = fdata[8]
			health.Relo = toInt(fdata[8])
			health.InitString = fdata[9]
			health.Init = toInt(fdata[9])
			health.UnassignString = fdata[10]
			health.Unassign = toInt(fdata[10])
			health.PendingTasksString = fdata[11]
			health.PendingTasks = toInt(fdata[11])
			health.MaxTaskWaitTime = fdata[12]
			health.ActiveShardsPercent = fdata[13]
		}
	}
	return health, nil
}

func (e Elasticsearch) Master() (Master, error) {
	data, err := e.cat_get("_cat/master?v")
	if err != nil {
		return Master{}, err
	}
	master := Master{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			master.Id = fdata[0]
			master.Host = fdata[1]
			master.Ip = fdata[2]
			master.Node = fdata[3]
		}
	}
	return master, nil
}

func (e Elasticsearch) NodeAttrs() ([]NodeAttr, error) {
	data, err := e.cat_get("_cat/nodeattrs?v")
	if err != nil {
		return nil, err
	}
	nodeattrs := []NodeAttr{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			nodeattr := NodeAttr{}
			nodeattr.Node = fdata[0]
			nodeattr.Host = fdata[1]
			nodeattr.Ip = fdata[2]
			nodeattr.Attr = fdata[3]
			nodeattr.Value = fdata[4]
			nodeattrs = append(nodeattrs, nodeattr)
		}
	}
	return nodeattrs, nil
}

func (e Elasticsearch) Nodes() ([]Node, error) {
	data, err := e.cat_get("_cat/nodes?v")
	if err != nil {
		return nil, err
	}
	nodes := []Node{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			node := Node{}
			node.Ip = fdata[0]
			node.HeapPercentString = fdata[1]
			node.HeapPercent = toInt(fdata[1])
			node.RamPercentString = fdata[2]
			node.RamPercent = toInt(fdata[2])
			node.CpuString = fdata[3]
			node.Cpu = toInt(fdata[3])
			node.Load1MString = fdata[4]
			node.Load1M = toFloat(fdata[4])
			node.Load5MString = fdata[5]
			node.Load5M = toFloat(fdata[5])
			node.Load15MString = fdata[6]
			node.Load15M = toFloat(fdata[6])
			node.NodeRole = fdata[7]
			if fdata[8] == "*" {
				node.Master = true
			}
			node.Name = fdata[9]
			nodes = append(nodes, node)
		}
	}
	return nodes, nil
}

func (e Elasticsearch) PendingTasks() ([]PendingTask, error) {
	data, err := e.cat_get("_cat/pending_tasks?v")
	if err != nil {
		return nil, err
	}
	pendingtasks := []PendingTask{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			pendingtask := PendingTask{}
			pendingtask.InsertOrderString = fdata[0]
			pendingtask.InsertOrder = toInt(fdata[0])
			pendingtask.TimeInQueue = fdata[1]
			pendingtask.Priority = fdata[2]
			pendingtask.Source = strings.Join(fdata[3:], " ")
			pendingtasks = append(pendingtasks, pendingtask)
		}
	}
	return pendingtasks, nil
}

func (e Elasticsearch) Plugins() ([]Plugin, error) {
	data, err := e.cat_get("_cat/plugins?v")
	if err != nil {
		return nil, err
	}
	plugins := []Plugin{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			plugin := Plugin{}
			plugin.Name = fdata[0]
			plugin.Component = fdata[1]
			plugin.Version = fdata[2]
			plugins = append(plugins, plugin)
		}
	}
	return plugins, nil
}

func (e Elasticsearch) Recovery() ([]Recovery, error) {
	data, err := e.cat_get("_cat/recovery?v")
	if err != nil {
		return nil, err
	}
	recovery := []Recovery{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			r := Recovery{}
			r.Index = fdata[0]
			r.ShardString = fdata[1]
			r.Shard = toInt(fdata[1])
			r.Time = fdata[2]
			r.Type = fdata[3]
			r.Stage = fdata[4]
			r.SourceHost = fdata[5]
			r.SourceNode = fdata[6]
			r.TargetHost = fdata[7]
			r.TargetNode = fdata[8]
			r.Repository = fdata[9]
			r.Snapshot = fdata[10]
			r.FilesString = fdata[11]
			r.Files = toInt(fdata[11])
			r.FilesRecoveredString = fdata[12]
			r.FilesRecovered = toInt(fdata[12])
			r.FilesPercent = fdata[13]
			r.FilesTotalString = fdata[14]
			r.FilesTotal = toInt(fdata[14])
			r.BytesString = fdata[15]
			r.Bytes = toInt(fdata[15])
			r.BytesRecoveredString = fdata[16]
			r.BytesRecovered = toInt(fdata[16])
			r.BytesPercent = fdata[17]
			r.BytesTotalString = fdata[18]
			r.BytesTotal = toInt(fdata[18])
			r.TranslogOpsString = fdata[19]
			r.TranslogOps = toInt(fdata[19])
			r.TranslogOpsRecoveredString = fdata[20]
			r.TranslogOpsRecovered = toInt(fdata[20])
			r.TranslogOpsPercent = fdata[21]
			recovery = append(recovery, r)
		}
	}
	return recovery, nil
}

func (e Elasticsearch) Repositories() ([]Repository, error) {
	data, err := e.cat_get("_cat/repositories?v")
	if err != nil {
		return nil, err
	}
	repositories := []Repository{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			repository := Repository{}
			repository.Id = fdata[0]
			repository.Type = fdata[1]
			repositories = append(repositories, repository)
		}
	}
	return repositories, nil
}

func (e Elasticsearch) ThreadPool() ([]ThreadPool, error) {
	data, err := e.cat_get("_cat/thread_pool?v")
	if err != nil {
		return nil, err
	}
	threadpools := []ThreadPool{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			threadpool := ThreadPool{}
			threadpool.NodeName = fdata[0]
			threadpool.Name = fdata[1]
			threadpool.ActiveString = fdata[2]
			threadpool.Active = toInt(fdata[2])
			threadpool.QueueString = fdata[3]
			threadpool.Queue = toInt(fdata[3])
			threadpool.RejectedString = fdata[4]
			threadpool.Rejected = toInt(fdata[4])
			threadpools = append(threadpools, threadpool)
		}
	}
	return threadpools, nil
}

func (e Elasticsearch) Shards() ([]Shard, error) {
	data, err := e.cat_get("_cat/shards?v")
	if err != nil {
		return nil, err
	}
	shards := []Shard{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			shard := Shard{}
			shard.Index = fdata[0]
			shard.ShardString = fdata[1]
			shard.Shard = toInt(fdata[1])
			shard.PriRep = fdata[2]
			shard.State = fdata[3]
			shard.DocsString = fdata[4]
			shard.Docs = toInt(fdata[4])
			shard.StoreString = fdata[5]
			shard.Store = toByte(fdata[5])
			shard.Ip = fdata[6]
			shard.Node = fdata[7]
			shards = append(shards, shard)
		}
	}
	return shards, nil
}

func (e Elasticsearch) Segments() ([]Segment, error) {
	data, err := e.cat_get("_cat/segments?v")
	if err != nil {
		return nil, err
	}
	segments := []Segment{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			segment := Segment{}
			segment.Index = fdata[0]
			segment.ShardString = fdata[1]
			segment.Shard = toInt(fdata[1])
			segment.PriRep = fdata[2]
			segment.Ip = fdata[3]
			segment.Segment = fdata[4]
			segment.GenerationString = fdata[5]
			segment.Generation = toInt(fdata[5])
			segment.DocsCountString = fdata[6]
			segment.DocsCount = toInt(fdata[6])
			segment.DocsDeletedString = fdata[7]
			segment.DocsDeleted = toInt(fdata[7])
			segment.SizeString = fdata[8]
			segment.Size = toByte(fdata[8])
			segment.SizeMemoryString = fdata[9]
			segment.SizeMemory = toInt(fdata[9])
			segment.Committed = toBool(fdata[10])
			segment.Searchable = toBool(fdata[11])
			segment.Version = fdata[12]
			segment.Compound = toBool(fdata[13])
			segments = append(segments, segment)
		}
	}
	return segments, nil
}

func (e Elasticsearch) Snapshots(repo string) ([]Snapshot, error) {
	data, err := e.cat_get(fmt.Sprintf("_cat/snapshots/%s?v", repo))
	if err != nil {
		return nil, err
	}
	snapshots := []Snapshot{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			snapshot := Snapshot{}
			snapshot.Id = fdata[0]
			snapshot.Status = fdata[1]
			snapshot.StartEpoch = toTime(fdata[2])
			snapshot.StartTime = fdata[3]
			snapshot.EndEpoch = toTime(fdata[4])
			snapshot.EndTime = fdata[5]
			snapshot.Duration = fdata[6]
			snapshot.IndicesString = fdata[7]
			snapshot.Indices = toInt(fdata[7])
			snapshot.SuccessfulShardsString = fdata[8]
			snapshot.SuccessfulShards = toInt(fdata[8])
			snapshot.FailedShardsString = fdata[9]
			snapshot.FailedShards = toInt(fdata[9])
			snapshot.TotalShardsString = fdata[10]
			snapshot.TotalShards = toInt(fdata[10])
			snapshots = append(snapshots, snapshot)
		}
	}
	return snapshots, nil
}

func (e Elasticsearch) Templates() ([]Template, error) {
	data, err := e.cat_get("_cat/templates?v")
	if err != nil {
		return nil, err
	}
	templates := []Template{}
	for _, line := range data {
		if len(line) > 0 {
			fdata := strings.Fields(line)
			template := Template{}
			template.Name = fdata[0]
			template.IndexPatterns = strings.Split(fdata[1][1:len(fdata[1])-1], ",")
			template.Order = toInt(fdata[2])
			if len(fdata) > 3 {
				template.Version = fdata[3]
			}
			templates = append(templates, template)
		}
	}
	return templates, nil
}
