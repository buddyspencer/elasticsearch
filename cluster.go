package elasticsearch

func (e Elasticsearch) ClusterHealth() (ClusterHealth, error) {
	clusterhealth := ClusterHealth{}
	err := e.cluster_get("_cluster/health", &clusterhealth)
	if err != nil {
		return ClusterHealth{}, err
	}
	return clusterhealth, nil
}

func (e Elasticsearch) ClusterSettings() (ClusterSettings, error) {
	clustersettings := ClusterSettings{}
	err := e.cluster_get("_cluster/settings?include_defaults=true", &clustersettings)
	if err != nil {
		return ClusterSettings{}, err
	}
	return clustersettings, nil
}
