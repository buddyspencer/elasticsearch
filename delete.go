package elasticsearch

import (
	"bytes"
	"net/http"
)

func (e Elasticsearch) DeleteIndex(index string) (http.Response, error) {
	body := bytes.NewBuffer([]byte{})
	return e.do_req(index, body, http.MethodDelete)
}
