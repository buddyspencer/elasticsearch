package elasticsearch

import (
	"bytes"
	"net/http"
)

func (e Elasticsearch) IndexExists(index string) (http.Response, error) {
	body := bytes.NewBuffer([]byte{})
	return e.do_req(index, body, http.MethodHead)
}
