package elasticsearch

import (
	"bytes"
	"code.cloudfoundry.org/bytefmt"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func (e Elasticsearch) cat_get(url string) ([]string, error) {
	content, err := e.get(url)
	if err != nil {
		return nil, err
	}
	data := strings.Split(content, "\n")
	data = data[1:]
	return data, nil
}

func (e Elasticsearch) get(url string) (string, error) {
	resp, err := e.Client.Get(fmt.Sprintf("%s%s", e.Url, url))
	if err != nil {
		return "", err
	}
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	resp.Body.Close()
	return string(content), nil
}

func (e Elasticsearch) cluster_get(url string, element interface{}) error {
	resp, err := e.Client.Get(fmt.Sprintf("%s%s", e.Url, url))
	if err != nil {
		return err
	}
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	resp.Body.Close()
	json.Unmarshal(content, &element)
	return nil
}

func (e Elasticsearch) do_req(url string, body io.Reader, method string) (http.Response, error) {
	req, err := http.NewRequest(method, fmt.Sprintf("%s%s", e.Url, url), body)
	if err != nil {
		return http.Response{}, err
	}

	resp, err := e.Client.Do(req)
	if err != nil {
		return http.Response{}, err
	}

	return *resp, err
}

func toInt(num string) int {
	n, err := strconv.Atoi(num)
	if err != nil {
		n = 0
	}
	return n
}

func toFloat(num string) float64 {
	n, err := strconv.ParseFloat(num, 64)
	if err != nil {
		n = 0
	}
	return n
}

func toByte(num string) uint64 {
	n, err := bytefmt.ToBytes(num)
	if err != nil {
		n = 0
	}
	return n
}

func toTime(num string) time.Time {
	i, err := strconv.ParseInt(num, 10, 64)
	if err != nil {
		return time.Time{}
	}
	return time.Unix(i, 0)
}

func toBool(num string) bool {
	i, err := strconv.ParseBool(num)
	if err != nil {
		return false
	}
	return i
}

func (e Elasticsearch) put(statuscode int, url string, jsondata []byte) (*http.Response, error) {
	put, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(jsondata))
	put.Header.Set("Content-Type", "application/json; charset=utf-8")
	resp, err := e.Client.Do(put)
	if err != nil {
		return resp, err
	}
	if resp.StatusCode != statuscode {
		return nil, errors.New(fmt.Sprintf("There was a problem. Errorcode from Elasticsearch %d", resp.StatusCode))
	}
	return resp, nil
}
