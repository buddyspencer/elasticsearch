package elasticsearch

import (
	"fmt"
)

func (e Elasticsearch) IndexSettings(index string) (IndexSettings, error) {
	var indexsettings map[string]IndexSettings
	err := e.cluster_get(fmt.Sprintf("%s/_settings/", index), &indexsettings)
	if err != nil {
		return IndexSettings{}, err
	}
	return indexsettings[index], nil
}
