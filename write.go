package elasticsearch

import (
	"encoding/json"
	"fmt"
	"github.com/rs/xid"
	"net/http"
)

func (e Elasticsearch) Send(data Document) (*http.Response, error) {
	if data.Id == "" {
		data.Id = xid.New().String()
	}
	url := fmt.Sprintf("%s%s/%s/%s", e.Url, data.Index, data.Type, data.Id)
	jsondata, err := json.Marshal(data.Fields)
	if err != nil {
		return nil, err
	}

	return e.put(201, url, jsondata)
}

func (e Elasticsearch) Bulk(data []Document) (*http.Response, error) {
	datastring := ""
	for _, log := range data {
		d := make(map[string]interface{})
		d["index"] = make(map[string]interface{})
		index_data := make(map[string]interface{})
		if log.Id == "" {
			log.Id = xid.New().String()
		}
		index_data["_index"] = log.Index
		index_data["_type"] = log.Type
		index_data["_id"] = log.Id
		d["index"] = index_data
		jsonData, err := json.Marshal(d)
		if err != nil {
			return nil, err
		}
		datastring += string(jsonData) + "\n"
		jsonData, err = json.Marshal(log.Fields)
		if err != nil {
			return nil, err
		}
		datastring += string(jsonData) + "\n"
	}
	url := fmt.Sprintf("%s_bulk", e.Url)

	return e.put(200, url, []byte(datastring))
}
